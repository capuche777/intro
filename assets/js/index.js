const img = document.querySelector('.img'); // Alcanza el div imagen, al cual se han aplicado los efectos
/**
 * 1.- Aplica los efectos fadeIn para que la imagen aparezca automaticamente
 * 2.- Elimina la clase y aplica el efecto de salida
 * 3.- Re-dirige la pagina de eleccion
 * Nota: Puede modificar los tiempos de aplicacion en milisegundos, *1000 - 1 segundo, 2000, dos segundos
 */

(() => {
        img.classList.add('fadeIn');
        setInterval(() => {
            img.classList.remove('fadeIn');
            img.classList.add('fadeOut');
            setInterval(() => {
                window.location.href = 'segundo.html' // <== Cambiar aqui la url de redireccion.
            }, 2000);
        }, 2000);
})();